console.log("ex 01 L08");
/* funzione ottimizzata per implementare l'ereditarietà */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// definisco la classe persona
var Persona = /** @class */ (function () {
    function Persona(name, age) {
        this.nome = name;
        this.eta = age;
    }
    Persona.prototype.getProfile = function () {
        var out = this.nome+" è una persona di "+this.eta+" anni";
        return out;
    };
    return Persona;
}());

// definisco la classe Atleta
/* estende Person */
var Atleta = /** @class */ (function (_super) {
    __extends(Atleta, _super);
    function Atleta(name, age, speed,selector_barra) {
        var _this = _super.call(this, name, age) || this;
        _this.velocita = speed;
        //
        _this.div_barra = $(selector_barra);
        _this.div_nome = this.div_barra.find(".nome");
        _this.div_metri = this.div_barra.find(".metri");
        //
        _this.initDivs();
        //
        return _this;
    }

    Atleta.prototype.div_barra;
    Atleta.prototype.div_nome;
    Atleta.prototype.div_metri;
    Atleta.prototype.metri_percorsi_totali = 0;

    /* sovrascrive il metodo aggiungendone funzionalità */
    Atleta.prototype.getProfile = function () {
        var out = _super.prototype.getProfile.call(this);
        out += " che corre a "+this.velocita+" metri al secondo";
        return out;
    };

    Atleta.prototype.initDivs = function () {
        this.div_nome.html(this.nome);
        this.div_metri.html(this.metri_percorsi_totali+"mt");
        this.div_barra.css({
            width: "",
            background: ""
        })
    };

    Atleta.prototype.corri = function (secondi) {
        // calcolo i metri percorsi
        var metri_percorsi_adesso = this.velocita*secondi;

        // definisco una variabile dove salvare la perdita di metri
        var perdita_mt = 0;
        var distanza = 0;
        // faccio un loop ogni 10 metri finchè non arrivo alla distanza
        while (distanza<=metri_percorsi_adesso) {
            perdita_mt += (distanza/10)/100*(0.2*this.eta);
            distanza += 10;
        }
        this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
        //
        this.updateBarra();
    };

    Atleta.prototype.updateBarra = function() {
        if (this.metri_percorsi_totali<500) {
            this.div_metri.html(Math.round(this.metri_percorsi_totali)+"mt");
            this.div_barra.css({
                width: Math.round(this.metri_percorsi_totali/500*100)+"%"
            });
        } else {
            this.div_metri.html("500mt");
            this.div_barra.css({
                width: "100%",
                background: "red"
            });
        }

    };

    return Atleta;
}(Persona));

// definisco la classe persona
var Gara = /** @class */ (function () {
    function Gara() {
        console.log("new Gara");
        this.div_descrizioni = $("#descrizioni");
        //
        this.pulsante = $("#start input");
        var _this = this;
        this.pulsante.on("click",function () {
            _this.clickPulsante();
        });
        //
        this.initAtleti();
        this.fillDescrizioni();
    }
    Gara.prototype.pulsante;
    Gara.prototype.atleta_1;
    Gara.prototype.atleta_2;
    Gara.prototype.atleta_3;
    Gara.prototype.atleti;
    Gara.prototype.div_descrizioni;
    Gara.prototype.interval_id;
    Gara.prototype.clickPulsante = function () {
        console.log("clickPulsante");
        console.log(this.atleti);
        //
        var _this = this;
        this.interval_id = setInterval(function () {
            _this.updateAtleti();
        },500);
        this.pulsante.off("click");
    };
    Gara.prototype.initAtleti = function () {
        var eta,velocita;
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    };
    Gara.prototype.generaNumeroCasuale = function (minimo, massimo, decimali) {
        var out = NaN;
        if (typeof decimali === "undefined") {
            decimali = 0;
        }
        var fattore_decimali = Math.pow(10,decimali);
        // calcolo la differenza
        var diff = (massimo*fattore_decimali)-(minimo*fattore_decimali);
        // generiamo un numero casuale
        var numero = Math.random();
        var var_casuale = Math.round(numero*diff);
        // ci assicuriamo che rientri nell'intervallo dato
        // restituiamo il numero
        out = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
        return out;
    };
    Gara.prototype.updateAtleti = function () {
        var gara_finita = false;
        for (var i=0; i<this.atleti.length; i++) {
            this.atleti[i].corri(10);
            if (this.atleti[i].metri_percorsi_totali>=500) {
                gara_finita = true;
            }
        }
        if (gara_finita) {
            clearInterval(this.interval_id);
            var _this = this;
            this.pulsante.on("click", function () {
                _this.riparti();
            });
        }

    };
    Gara.prototype.riparti = function () {
        this.reset();
        var _this = this;
        this.interval_id = setInterval(function () {
            _this.updateAtleti();
        },500);
        this.pulsante.off("click");
    };
    Gara.prototype.reset = function () {
        console.log("reset");
        this.initAtleti();
        this.fillDescrizioni();
    };
    Gara.prototype.fillDescrizioni = function () {
        console.log("fillDescrizioni");
        for (var i=0; i<this.atleti.length; i++) {
            var atleta = this.atleti[i];
            console.log(atleta);
            var div = this.div_descrizioni.find(".atleta"+(i+1));
            div.html(atleta.getProfile());
        }
    };
    return Gara;
}());

var gara = new Gara();

/*
// creo tre istanze di Atleta
var atleta_1 = new Atleta("Giovanni",36,6);
var atleta_2 = new Atleta("Andrea",29,7);
var atleta_3 = new Atleta("Anna",18,8);

// sostituisco le descrizioni
var descrizioni = $("#descrizioni");
descrizioni.find(".atleta1").html(atleta_1.getProfile());
descrizioni.find(".atleta2").html(atleta_2.getProfile());
descrizioni.find(".atleta3").html(atleta_3.getProfile());



var aggiornaBarre = function () {
    // sostituisco i valori nelle barre
    atleta_1.corri(10);
    atleta_2.corri(10);
    atleta_3.corri(10);

    var atleta;

    atleta = $("#gara .atleta1");
    atleta.find(".nome").html(atleta_1.nome);
    atleta.find(".metri").html(atleta_1.metri_percorsi_totali+"mt");

    atleta = $("#gara .atleta2");
    atleta.find(".nome").html(atleta_2.nome);
    atleta.find(".metri").html(atleta_2.metri_percorsi_totali+"mt");

    atleta = $("#gara .atleta3");
    atleta.find(".nome").html(atleta_3.nome);
    atleta.find(".metri").html(atleta_3.metri_percorsi_totali+"mt");

    // controllo se un'atleta è arrivato a 500mt
    if (atleta_1.metri_percorsi_totali>=500) {
        $("#gara .atleta1").css({
            width: "100%",
            background: "red"
        });
        pulsante.off("click",aggiornaBarre);
        $("#gara .atleta1 .metri").html("500mt");
        return;
    }
    if (atleta_2.metri_percorsi_totali>=500) {
        $("#gara .atleta2").css({
            width: "100%",
            background: "red"
        });
        pulsante.off("click",aggiornaBarre);
        $("#gara .atleta2 .metri").html("500mt");
        return;
    }
    if (atleta_3.metri_percorsi_totali>=500) {
        $("#gara .atleta3").css({
            width: "100%",
            background: "red"
        });
        pulsante.off("click",aggiornaBarre);
        $("#gara .atleta3 .metri").html("500mt");
        return;
    }

    // aggiorno la larghezza delle barre
    var larghezza;

    larghezza = atleta_1.metri_percorsi_totali/500*100;
    console.log(larghezza);
    $("#gara .atleta1").css({width: larghezza+"%"});

    larghezza = atleta_2.metri_percorsi_totali/500*100;
    console.log(larghezza);
    $("#gara .atleta2").css({width: larghezza+"%"});

    larghezza = atleta_3.metri_percorsi_totali/500*100;
    console.log(larghezza);
    $("#gara .atleta3").css({width: larghezza+"%"});
};


var pulsante = $("#start input");
pulsante.on("click",aggiornaBarre);
*/
